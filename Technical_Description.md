# Fisheye - Description technique - 02/05/2020

## Logiciels utilisés:

À noter que les 3 participants utilisent un ordinateur portable Mac (macOS Sierra, High Sierra et Catalina)

1. Jitsi Meet: vidéoconférence. 

	Utilisé pour la réception des flux audio et vidéo. 

	Une salle Jitsi Meet a été créé afin d'héberger la vidéoconférence nécessaire pour l'oeuvre. Cette salle peut être accéder en allant simplement sur l'URL suivant: https://meet.jit.si/CtrlNTestRoom

	Tous les flux sont reçus sur l'ordinateur de Francis. L'ordinateur de Francis ne transmet rien (vidéo et micro désactivés)
	- l'audio de Joannie est transmis par l'ordinateur portable de Joannie (caméra désactivée)
	- la vidéo de Joannie est transmise par le cellulaire de Joannie avec l'application Jitsi Meet (micro désactivé)
	- la vidéo d'Audrey est transmise par son ordinateur portable (micro désactivé)
	- la vidéo de Francis est transmis par son téléphone cellulaire avec l'application Jitsi Meet (micro désactivé)

	Les trois membres voient les vidéo de chacun, et n'entendent que l'audio transmis par Joannie. Aucun autre son n'est transmis.
	Sur l'ordinateur de Francis, la fenêtre Jitsi est en plein écran sur un second moniteur.

	De plus, afin de garantir une qualité audio supérieure, Joannie a accédé à la salle Jitsi en utilisant un URL spécial: cet URL tire profit de l'API de Jitsi et permet de désactiver certaines options qui dégradent la qualité du son envoyé, comme par exemple un filtre passe-haut, un denoiser, une gate audio, etc.

	L'URL spécial utilisé à cette fin est le suivant: 
	https://meet.jit.si/CtrlNTestRoom#config.p2p.enabled=false&config.disableAP=true&config.disableAEC=false&config.disableNS=true&config.disableAGC=true&config.disableHPF=true&config.stereo=true

2. vMixDesktopCapture: capture d'écran NDI. 
	
	Utilisé pour capturer l'écran ou se trouve la fenêtre de Jitsi Meet (en plein écran).
	L'application capture l'entièreté d'un écran et le transmet en NDI. Lien: https://www.vmix.com/software/download.aspx

3. TouchDesigner: traitement vidéo en direct.
	
	À l'intérieur de TD, le flux NDI envoyé par vMix est séparé en 3 vidéos distinctes, à l'aide de Crop TOPs, permettant d'aller cadrer chaque membre de l'équipe séparément et d'appliquer des effets sur chaque vidéo de manière individuelle. Il est important que Jitsi soit en plein écran, car de cette manière, les fenêtres individuelles de chaque participant sont plus grandes: la résolution globale sera donc meilleure.

	La vidéo finale est connectée à un NDI Output TOP, et envoyé sur le réseau local en NDI.

4. Soundflower/Blackhole: piping audio d'une application à une autre.

	Soundflower et Blackhole sont deux interface audio virtuelles permettant d'acheminer l'audio d'une application à une autre. Leur fonctionnement est identique, mais Soundflower n'est plus maintenu. 

	Blackhole est une alternative plus moderne. Blackhole a été utilisé par Joannie, afin de transférer l'audio d'Ableton live directement dans l'entrée audio de Jitsi.

	Soundflower a été utilisé par Francis, pour acheminer l'audio sortant de Jitsi directement dans une entrée audio d'OBS.

5. OBS: streaming sur Facebook Live.

	Une scène OBS contenant 2 sources a été créée: 1 source vidéo NDI, récupérant le flux de TouchDesigner, et 1 entrée audio utilisant Soundflower, permettant de récupérer l'audio de Jitsi. 

	Le tout est directement streamé à partir d'OBS sur Facebook Live.

6. Ableton Live: création audio.

	Ableton Live a été utilisé par Joannie pour créer et activer en direct de la musique pendant la performance.

## Autres:

- Une lentille est posée manuellement sur la webcam intégrée à l'ordinateur d'Audrey, afin d'obtenir un effet fisheye.

- Un contrôleur MIDI Launchcontrol XL est utilisé par Francis afin de contrôler en direct certains paramètres vidéo de Touchdesigner

- Afin de pouvoir monitorer son audio, Joannie a configuré un Périphérique à sortie multiple (PSM) comme carte de son de sortie pour Ableton Live. Ce PSM étant configuré pour dupliquer l'audio en entrée sur Blackhole et sur la carte de son de Joannie, elle a pu monitorer le son produit tout en l'envoyant directement dans l'entrée audio de Jitsi (étant configuré pour prendre Balckhole comme entrée audio)

- Audrey a connecté son ordinateur portable sur sa télévision et ouvert Facebook sur cet écran, afin de pouvoir regarder notre propre stream Facebook en direct et ainsi pouvoir lire les commentaires du public.