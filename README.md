# Fisheye

## Summary

**Fisheye** is an online work created by choreograph/dancer Audrey Gaussiran, composer/percussionist Joannie Labelle and creative coder Francis Lecavalier, under the *[Ctrl+Alt+Del]* moniker. It is a live dance performance in four acts with live music with Ableton Live and real-time visual processing using [TouchDesigner](https://derivative.ca/). 

## Context

**Fisheye** premiered on May 2nd 2020 as part of the first edition of the *[Festival de Troi(e)s](https://petittheatre.org/fat-event/festival-de-troies/)* and won the festival’s Audience Award. It is the result of a 72-hour creative sprint hosted by the festival that ran from April 29th to May 2nd 2020. As part of this sprint, three teams of three contestants had to create, rehearse and perform a brand new audiovisual work leveraging videoconferencing tools and live audience interaction. The results were presented on a live Facebook stream hosted by the festival. The main theme of this first edition was “le bocal” (“the jar” or “the fish bowl”).

## Repo Description

Inside this repo, you'll find:

* The *.toe* files used to manipulate visuals in real-time with TouchDesigner (inside the `/TouchDesigner` folder)
* The *CtrlAltDel_STREAM.json* scene collection for [OBS Studio](https://obsproject.com/), which was used to live stream the performance on Facebook (or any other streaming platform).
* The title cards used in the OBS session (*Image_Intro.png* and *Image_Outro.png*)
* The *Technical_Description.md* file contains a full description of **Fisheye**'s technical setup. It lists all the required software and explains in detail how they all communicate and fit together (in French only).
